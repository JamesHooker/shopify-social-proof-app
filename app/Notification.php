<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'product_id',
        'type',
        'message',
        'period'
    ];
}
