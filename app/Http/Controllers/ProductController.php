<?php

namespace App\Http\Controllers;

use App\Product;
use App\Notification;
use App\Jobs\FetchProducts;
use App\Jobs\FetchOrders;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Show the product list in the dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('products', ['products' => Product::with('orders')->get()]);
    }

    /**
     * Show the application dashboard.
     */
    public function show($id)
    {

        $product = Product::findOrFail($id);

        $message = $product->recentOrders('48 hours')->count();
        $message .= ' People have purchased this item in the last 48 hours!';

        // Record Message shown
        Notification::create([
            'product_id' => $id,
            'type' => 'unique_orders',
            'message' => $message,
            'period' => '48 hours'
        ]);

        return [
            [
                'message' => $message
            ]
        ];
    }

    /**
     * Fire the event to fetch the latest orders
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        FetchProducts::dispatchNow();
        FetchOrders::dispatchNow();
        return redirect('products');
    }
}
