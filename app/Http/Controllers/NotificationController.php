<?php

namespace App\Http\Controllers;

use App\Jobs\PushScripts;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('notifications');
    }

    /**
     * Add the scripts and complete the connection to shopify.
     */
    public function connect()
    {
        PushScripts::dispatchNow();
        return redirect('notifications');
    }

}
