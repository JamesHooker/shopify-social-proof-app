<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id', 'product_id', 'quantity', 'order_time'
    ];

    protected $casts = [
        'order_time' => 'datetime'
    ];
}
