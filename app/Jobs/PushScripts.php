<?php

namespace App\Jobs;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PushScripts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $shopifyApi = app('ShopifyAPI');


        if (count(\Config::get('esc_shopify.script_tags')) > 0) {

            foreach (\Config::get('esc_shopify.script_tags') as $url) {

                $existingScripts = $shopifyApi->call('GET', '/admin/api/2020-04/script_tags.json', ['src' => $url])->script_tags;

                if (count($existingScripts) != 0) {
                    
                    foreach($existingScripts as $script) {
                        $shopifyApi->call('DELETE', '/admin/api/2020-04/script_tags/'. $script->id .'.json');
                    }

                }

                $shopifyApi->call('POST', '/admin/api/2020-04/script_tags.json', 
                        [
                            'script_tag' => [
                                'event' => 'onload',
                                'src' => $url
                            ]
                        ]
                    );

            }

        }

    }
}