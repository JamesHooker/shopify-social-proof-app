<?php

namespace App\Jobs;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $shopifyApi = app('ShopifyAPI');
        $products = $shopifyApi->call('GET', '/admin/api/2020-04/products.json');

        foreach($products->products as $product) {
            Product::UpdateOrCreate(
                [
                    'id' => $product->id
                ],
                [
                    'name' => $product->title
                ]
            );

        }

    }
}
