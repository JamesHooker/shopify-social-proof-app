<?php

namespace App\Jobs;

use App\OrderItem;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class FetchOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $shopifyApi = app('ShopifyAPI');
        $orders = $shopifyApi->call('GET', '/admin/api/2020-04/orders.json');

        foreach($orders->orders as $order) {

            foreach($order->line_items as $item) {

                // var_dump($order); die;

                OrderItem::UpdateOrCreate(
                    [
                        'order_id' => $order->id,
                        'product_id' => $item->product_id
                    ],
                    [
                        'order_time' => Carbon::parse($order->created_at),
                        'quantity' => $item->quantity
                    ]
                );

            }
        }

    }
}
