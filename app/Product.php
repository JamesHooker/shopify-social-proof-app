<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

class Product extends Model
{
    protected $fillable = [
        'id', 'name'
    ];

    /**
     * Get the orders for the product.
     */
    public function orders()
    {
        return $this->hasMany('App\OrderItem');
    }

    /**
     * Get the orders for the product.
     */
    public function recentOrders($period = '48 hours')
    {
        return $this->orders()->where(function (Builder $query) {
            return $query->where('order_time', '>', Carbon::now()->sub('48 hours'))
                         ->where('order_time', '<', Carbon::now());
        });
    }

    
}
