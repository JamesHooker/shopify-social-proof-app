
// This function will be injected by shopify for onload
// meta is defined on page

(() => {

    const injectStyles = () => {
        const head  = document.getElementsByTagName('head')[0];
        let link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = 'https://shopifypop.builtbyjames.co.uk/inject/css/storefront.css';
        link.media = 'all';
        head.appendChild(link);
    }
    
    const fetchNotifications = product => {
        fetch(`https://cors-anywhere.herokuapp.com/https://shopifypop.builtbyjames.co.uk/api/product/${product}`,
            {
                method: 'GET',
                mode: 'cors',
                header: {
                    "Access-Control-Allow-Origin" : "*", 
                    "Access-Control-Allow-Credentials" : true,
                }
            })
            .then(response => response.json())
            .then(notifications => {
                renderNotifications(notifications)
            });
    }

    const renderNotifications = notifications => {

        let notificationsHTML = document.createElement('div');
        notificationsHTML.className = 'spa-notes';

        Array.from(notifications, notification => {
            notificationsHTML.appendChild(buildNotifcation(notification));
        });

        // Push to DOM
        document.body.appendChild(notificationsHTML);

    }

    const buildNotifcation = notification => {

        /*
            <div class="spa-notes__notification">
                <h3 class="spa-notes__notification__title">{message}</h3>
            </div>
        */

       let wrapper = document.createElement('div');
       wrapper.className = 'spa-notes__notification';

       let text = document.createElement('h3');
       text.className = 'spa-notes__notification__title';
       const messageNode = document.createTextNode(notification.message); 
       text.appendChild(messageNode);

       wrapper.appendChild(text);

       return wrapper;
    }

    if(meta && meta?.product) {

        injectStyles()
        fetchNotifications(meta.product.id)

    }

})();
