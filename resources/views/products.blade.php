@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Products</div>

                <div class="card-body">
                    <form method="POST" action="/products">
                        @csrf
                        <button>Refresh</button>
                    </form>

                    <table class="table table-bordered mt-5">
                        <tr>
                            <th>Product</th>
                            <th>Orders</th>
                            <th>Sold</th>
                            <th>Sold / 48 hours</th>
                        </tr>

                        @foreach ($products as $product)
                        <tr>
                            <td>
                                <h3>{{ $product->name }}</h3>
                                {{ $product->id }}
                            </td>
                            <td>{{ $product->orders->count() }}</td>
                            <td>{{ $product->orders->sum('quantity') }}</td>
                            <td>{{ $product->recentOrders()->count() }}</td>
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
